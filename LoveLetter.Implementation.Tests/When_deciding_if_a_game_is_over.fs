﻿namespace LoveLetter.Implementation.Tests
module When_deciding_if_a_game_is_over =
    open NUnit.Framework
    open Swensen.Unquote
    open LoveLetter.Implementation.Models
    open LoveLetter.Implementation.Game
    open LoveLetter.Implementation.Players

    [<Test>]
    let ``And there is only one player left, then the game should end`` () =
        let game = {deck = [Guard]; players=[createPlayer Guard 1]; setAsideCard=Baron}
        test <@ shouldGameBeOver game = OnePlayerStanding @>

    [<Test>]
    let ``And the deck is out of cards, the game should end`` () =
        let game = {deck =[]; players =[createPlayer Guard 1; createPlayer Princess 2]; setAsideCard=Baron}
        test <@ shouldGameBeOver game = DeckOutOfCards @>

    [<Test>]
    let ``And there are two players left playing, and the deck is not empty, then the game should continue`` () =
        let game = {deck=[Guard; Priest;]; players=[createPlayer Guard 1; createPlayer Baron 2]; setAsideCard=King}
        test <@ shouldGameBeOver game = Continue @>

