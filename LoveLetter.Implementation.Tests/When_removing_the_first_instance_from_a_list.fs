﻿namespace LoveLetter.Implementation.Tests
module When_removing_the_first_instance_from_a_list =
    open NUnit.Framework
    open Swensen.Unquote
    open LoveLetter.Implementation.Infrastructure

    [<Test>]
    let ``And the list is empty, then an empty list is returned`` () =
        test <@ removeFirstOccurence 2 [] = [] @>

    [<Test>]
    let ``And the list is [2] and we're removing 3, then [2] is returned`` () =
        test <@ removeFirstOccurence 3 [2] = [2] @>

    [<Test>]
    let ``And the list is [2] and we're removing 2, then [] is returned`` () =
        test <@ removeFirstOccurence 2 [2] = [] @>

    [<Test>]
    let ``And the list is [2;3] and we're removing 1, then [2;3] is returned`` () =
        test <@ removeFirstOccurence 1 [2;3] = [2;3] @>

    [<Test>]
    let ``And the list is [2;2] and we're removing 2, then [2] is returned`` () =
        test <@ removeFirstOccurence 2 [2;2] = [2] @>

