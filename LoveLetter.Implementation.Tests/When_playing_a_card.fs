﻿namespace LoveLetter.Implementation.Tests
module When_playing_a_card =
    open LoveLetter.Implementation.Models
    open LoveLetter.Implementation.Actions
    open Swensen.Unquote
    open NUnit.Framework
    open LoveLetter.Implementation

    let createPlayer card = {card=card; playerNumber=1; isProtected=false; isOutOfGame=false}

    [<Test>]
    let ``When playing the guard, and the opponent has the card, then the opponent is out of the game`` () =
        let player = createPlayer Baron

        test <@ guardAction player Baron = {player with isOutOfGame=true} @>

    [<Test>]
    let ``When playing the guard, and the opponent does not have the card, then the opponent is still in the game`` () =
        let player = createPlayer Baron

        test <@guardAction player Princess = player @>

    [<Test>]
    let ``When playing the priest, then the opponent's card is returned`` () =
        let player = createPlayer Princess

        test <@priestAction player = Princess@>

    [<Test>]
    let ``When playing the baron, and both players have the same card, then neither player is eliminated`` () =
        let playerOne = createPlayer Priest
        let playerTwo = createPlayer Priest

        test<@baronAction playerOne playerTwo = (playerOne, playerTwo)@>

    [<Test>]
    let ``When playing the baron, and player 1 has the higher card, then player 2 is eliminated`` () =
        let playerOne = createPlayer Princess
        let playerTwo = createPlayer Priest
        
        test <@baronAction playerOne playerTwo = (playerOne, {playerTwo with isOutOfGame=true}) @>

    [<Test>]
    let ``When playing the baron, and player 1 has the lower card, then player 1 is eliminated`` () =
        let playerOne = createPlayer Baron
        let playerTwo = createPlayer King

        test<@baronAction playerOne playerTwo = ({playerOne with isOutOfGame=true}, playerTwo)@>

    [<Test>]
    let ``When playing the handmaid, the player is now protected`` () =
        let player = createPlayer Priest

        test<@handmaidAction player = {player with isProtected=true}@>

    [<Test>]
    let ``When playing the prince, the player gets a new card`` () =
        let player = createPlayer Prince
        test<@princeAction player Princess = {player with card=Princess}@>

    [<Test>]
    let ``When playing the king, the players trade hands`` () =
        let playerOne = createPlayer Priest
        let playerTwo = createPlayer Handmaid

        let newPlayerOne, newPlayerTwo = kingAction playerOne playerTwo
        test<@ kingAction playerOne playerTwo = ({playerOne with card=playerTwo.card}, {playerTwo with card=playerOne.card})@>

    [<Test>]
    let ``When playing the countess, nothing happens`` () =
        countessAction ()

    [<Test>]
    let ``When playing the princess, the player is eliminated`` () =
        let player = createPlayer Card.Guard

        test<@princessAction player = {player with isOutOfGame=true}@>