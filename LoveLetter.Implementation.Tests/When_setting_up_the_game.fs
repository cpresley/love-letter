﻿namespace LoveLetter.Implementation.Tests
module When_setting_up_the_game =
    open NUnit.Framework
    open LoveLetter.Implementation.Models
    open LoveLetter.Implementation.Game
    open Swensen.Unquote
    open LoveLetter.Implementation.Players

    [<Test>]
    let ``And there are more players than cards, then None is returned`` () =
        let deckPrepare () = [Guard]
        let numPlayerGetter (options:int list)= Some 2

        test <@ None = initializeGame deckPrepare numPlayerGetter createPlayer @>

    [<Test>]
    let ``And there are the same number of cards as players, then None is returned`` () =
        let deckPrepare () = [Guard; Baron]
        let numPlayerGetter (options:int list) = Some 2

        test <@ None = initializeGame deckPrepare numPlayerGetter createPlayer @>

    [<Test>]
    let ``And there are more cards then players, then the game is constructed correctly`` () =
        let numPlayerGetter options = Some 4
        let deckPreparer () = [Guard; Priest; Baron; Princess; Countess;]
        
        let game = initializeGame deckPreparer numPlayerGetter createPlayer |> Option.get

        let expectedPlayers = [createPlayer Guard 1; createPlayer Priest 2; createPlayer Baron 3; createPlayer Princess 4]
        test <@ game = {deck=[]; setAsideCard=Countess; players=expectedPlayers} @>
        

