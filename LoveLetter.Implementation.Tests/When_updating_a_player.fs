﻿namespace LoveLetter.Implementation.Tests
module When_updating_a_player =
    open NUnit.Framework
    open Swensen.Unquote
    open LoveLetter.Implementation.Models
    open LoveLetter.Implementation.Players

    let player = createPlayer Guard 1
    
    [<Test>]
    let ``By creating the player, then they have the correct number, the correct card, in the game, and not protected`` () =
        test <@ player = {card=Guard; playerNumber =1; isOutOfGame=false; isProtected=false} @>

    [<Test>]
    let ``By giving them a new card, then they have the new card`` () =
        test <@ player |> changeCard Princess = {player with card=Princess} @>

    [<Test>]
    let ``By protecting them, then the property is set correctly`` () =
        test <@ player |> protectPlayer = {player with isProtected = true} @>

    [<Test>]
    let ``By knocking them out, then the property is set correctly`` () =
        test <@ player |> knockOutPlayer = {player with isOutOfGame=true} @>
