﻿namespace LoveLetter.Implementation.Tests
module BuildingTheDeckTests =
    open Swensen.Unquote
    open NUnit.Framework
    open LoveLetter.Implementation.Cards
    open LoveLetter.Implementation.Models

    let deck = prepareDeck ()

    let getCountOfElement list element =
        list |> List.filter (fun x -> x = element) |> List.length

    let countNumberOfCardsInDeck = prepareDeck () |> getCountOfElement

    [<Test>]
    let ``Then there are 16 cards in the deck`` () =
        test <@ deck |> List.length = 16 @>

    [<Test>]
    let ``Then there are 5 guards`` () =
        test <@ countNumberOfCardsInDeck Guard = 5@>

    [<Test>]
    let ``Then there are 2 priests`` () =
        test <@ countNumberOfCardsInDeck Priest = 2@>

    [<Test>]
    let ``Then there are 2 barons`` () =
        test <@ countNumberOfCardsInDeck Baron = 2@>

    [<Test>]
    let ``Then there are 2 handmaids`` () =
        test <@ countNumberOfCardsInDeck Handmaid = 2@>

    [<Test>]
    let ``Then there are 2 princes`` () =
        test <@ countNumberOfCardsInDeck Prince = 2@>

    [<Test>]
    let ``Then there is 1 king`` () =
        test <@ countNumberOfCardsInDeck King = 1@>

    [<Test>]
    let ``Then there is 1 countess`` () =
        test <@ countNumberOfCardsInDeck Countess = 1@>

    [<Test>]
    let ``Then there is 1 princess`` () =
        test <@ countNumberOfCardsInDeck Princess = 1@>