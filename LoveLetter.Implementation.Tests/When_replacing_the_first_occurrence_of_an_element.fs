﻿namespace LoveLetter.Implementation.Tests
module When_replacing_the_first_occurrence_of_an_element =
    open NUnit.Framework
    open Swensen.Unquote
    open LoveLetter.Implementation.Infrastructure

    [<Test>]
    let ``And the list is empty, then an empty list is returned`` () =
        test <@ replaceFirstOccurrence 2 5 [] = [] @>

    [<Test>]
    let ``And the list is [2] and replacing 1 with 3, then [2] is returned`` () =
        test <@ replaceFirstOccurrence 1 3 [2] = [2] @>

    [<Test>]
    let ``And the list is [2] and replacing 2 with 3, then [3] is returned`` () =
        test <@ replaceFirstOccurrence 2 3 [2] = [3] @>

    [<Test>]
    let ``And the list is [2;3] and replacing 1 with 3, then [2;3] is returned`` () =
        test <@ replaceFirstOccurrence 1 3 [2;3] = [2;3] @>

    [<Test>]
    let ``And the list is [2;3] and replacing 2 with 3, then [3;3] is returned`` () =
        test <@ replaceFirstOccurrence 2 3 [2;3] = [3;3] @>

    [<Test>]
    let ``And the list is [2;2] and replacing 2 with 3, then [3;2] is returned`` () =
        test <@ replaceFirstOccurrence 2 3 [2;2] = [3;2] @>

