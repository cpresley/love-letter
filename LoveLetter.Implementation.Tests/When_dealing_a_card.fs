﻿namespace LoveLetter.Implementation.Tests
module When_dealing_a_card =
    open NUnit.Framework
    open Swensen.Unquote
    open LoveLetter.Implementation.Cards
    open LoveLetter.Implementation.Models

    [<Test>]
    let ``And the deck is empty, then None is returned`` () =
        test<@ drawACard [] = (None,[]) @>

    [<Test>]
    let ``And the deck only has one card, then that card with an empty list is returned`` ()  =
        test<@ drawACard [Guard] = (Some Guard, []) @>

    [<Test>]
    let ``And there are more than one card in the deck, then that card with the rest of the deck is returned`` () =
        test<@ drawACard [Guard; Princess; Priest] = (Some Guard, [Princess; Priest]) @>

