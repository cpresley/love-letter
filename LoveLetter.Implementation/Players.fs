﻿namespace LoveLetter.Implementation
module Players =
    open Models

    let createPlayer card playerNumber = {card=card; playerNumber=playerNumber; isOutOfGame = false; isProtected=false}
    let changeCard card player = 
        match player.card with
        | Princess -> {player with isOutOfGame = true}
        | _ -> {player with card=card}
    let protectPlayer player = {player with isProtected=true}
    let knockOutPlayer player = {player with isOutOfGame=true}
