﻿namespace LoveLetter.Implementation
module Actions =
    open Models
    open Cards
    open Players
    open Infrastructure
    open Prompts

    let guardAction player card = if player.card = card then player |> knockOutPlayer else player
    let playGuardCard filteredPlayers game =
        let cards = cardTypes () |> List.filter (fun x -> x <> Guard)
        let getGuardSelection players cards = userNeedsToChooseATarget players , userNeedsToChooseACard cards
        let player,card = getGuardSelection filteredPlayers cards
        match player with
        | Some x -> {game with players = game.players |> replaceFirstOccurrence x (guardAction x (card |> Option.get))}
        | None -> game

    let priestAction player = player.card
    let playPriestCard filteredPlayers game =
        let target = userNeedsToChooseATarget filteredPlayers
        match target with
        | Some x -> printfn "Player %i has %A" x.playerNumber (priestAction x)
        | None -> ()
        game
    
    let baronAction playerA playerB =
        let pOnePower = playerA.card |> cardPower
        let pTwoPower = playerB.card |> cardPower

        if pOnePower = pTwoPower then playerA, playerB
        elif pOnePower > pTwoPower then playerA, playerB |> knockOutPlayer
        else playerA |> knockOutPlayer, playerB

    let playBaronCard filteredPlayers game =
        let playerA = game.players.Head
        let playerB = userNeedsToChooseATarget filteredPlayers
        match playerB with
        | Some x -> 
            let playerA',playerB' = baronAction playerA x
            {game with players = game.players |> replaceFirstOccurrence playerA playerA' |> replaceFirstOccurrence x playerB'}
        | None -> game

    let handmaidAction player = player |> protectPlayer
    let playHandmaidCard game =
        let player' = handmaidAction game.players.Head
        {game with players = game.players |> replaceFirstOccurrence game.players.Head player'}

    let princeAction player card = player |> changeCard card 
    let playPrinceCard filteredPlayers game = 
        let player = userNeedsToChooseATarget filteredPlayers |> Option.get
        let nextCard, deck' = drawACard game.deck
        let player' = match nextCard with
                        | Some x -> princeAction player x
                        | None -> princeAction player game.setAsideCard
        {game with players = game.players |> replaceFirstOccurrence player player'}
    
    let kingAction playerOne playerTwo = 
        playerOne |> changeCard playerTwo.card, playerTwo |> changeCard playerOne.card
    let playKingCard players game =
        let player = userNeedsToChooseATarget players
        match player with
            | Some x -> 
                let currentPlayer = game.players.Head
                let playerA, playerB = kingAction currentPlayer x
                {game with players = players |> replaceFirstOccurrence currentPlayer playerA |> replaceFirstOccurrence x playerB}
            | None -> game

    let countessAction () = ()
    
    let princessAction player = player |> knockOutPlayer
    let playPrincessCard game =
        let player = game.players |> List.head
        {game with players = game.players |> replaceFirstOccurrence player (princessAction player)}

    let playCard card game =
        let filteredPlayers = game.players |> List.filter (fun x -> not x.isOutOfGame && not x.isProtected && x <> game.players.Head) |> List.sortBy(fun y -> y.playerNumber)
        match card with
        | Guard -> playGuardCard filteredPlayers game
        | Priest -> playPriestCard filteredPlayers game
        | Baron -> playBaronCard filteredPlayers game
        | Handmaid -> playHandmaidCard game
        | Prince -> playPrinceCard filteredPlayers game
        | King -> playKingCard filteredPlayers game
        | Princess -> playPrincessCard game
        | _ -> game