﻿namespace LoveLetter.Implementation
module Prompts =
    open Models
    open Cards
    open System

    let promptForTarget players =
        printfn "Please choose a player to target."
        players |> List.sortBy (fun x -> x.playerNumber) |> List.iter (fun y -> printfn "%i) Player %i" y.playerNumber y.playerNumber)
    let validatePlayerSelection selection choices =
        match selection |> Int32.TryParse with
        | (false, _) -> None
        | (true, value) -> choices |> List.tryFind (fun x -> x.playerNumber = value)

    let promptforCard cards =
        printfn "Please choose a card."
        cards |> List.map (fun x -> cardPower x) |> List.zip(cards) |> List.iter (fun cardAndNumber -> printfn "%i %A" (snd cardAndNumber) (fst cardAndNumber))
    let validateCardSelection selection choices =
        match selection |> Int32.TryParse with
        | (false, _) -> None
        | (true, value) -> choices |> List.tryFind (fun x -> cardPower x = value)

    let promptForPlayers choices =
        printf "How many players ("
        choices |> List.iter (fun x -> printf "%A, " x)
        printfn ")?" 
    
    let validateNumberOfPlayers selection choices =
        match selection |> Int32.TryParse with
        | (false, _) -> None
        | (true, value) -> choices |> List.tryFind (fun x -> x = value)

    let rec private userMakesAChoice prompt selectionValidator options =
        match options with
        | [] -> None
        | _ -> 
            prompt options
            let result = Console.ReadLine()
            match selectionValidator result options with
            | None -> userMakesAChoice prompt selectionValidator options
            | Some x -> Some x

    let userNeedsToChooseATarget targets = userMakesAChoice promptForTarget validatePlayerSelection targets
    let userNeedsToChooseACard cards = userMakesAChoice promptforCard validateCardSelection cards
    let userNeedsToChooseNumberOfPlayers options = userMakesAChoice promptForPlayers validateNumberOfPlayers options    
