﻿namespace LoveLetter.Implementation
module Infrastructure =
    let rec replaceFirstOccurrence oldElement newElement list =
        match list with
        | h::t -> if h = oldElement then newElement::t else h::(replaceFirstOccurrence oldElement newElement t)
        | _ -> []

    let rec removeFirstOccurence element list =
        match list with
        | h::t -> if h = element then t else h::removeFirstOccurence element t
        | _ -> []
