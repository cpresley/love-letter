﻿namespace LoveLetter.Implementation
module Game =
    open Models
    open Players
    open Cards
    open Infrastructure
    open Prompts
    open Actions

    let initializeGame deckGetter numPlayerGetter playerCreator =
        let deck = deckGetter ()
        let numPlayers = numPlayerGetter [2;3;4] |> Option.get
        if deck |> List.length <= numPlayers then None
        else
            let initialHands = deck |> List.toSeq |> Seq.truncate numPlayers |> Seq.toList
            let initialDeck = deck |> List.toSeq |> Seq.skip numPlayers |> Seq.toList
            let players = [1 .. numPlayers]|> List.zip initialHands  |> List.map (fun x ->  playerCreator (fst x) (snd x))
            Some {players = players; deck=initialDeck.Tail; setAsideCard=initialDeck.Head}

    let shouldGameBeOver game =
        if game.players |> List.filter (fun x -> x.isOutOfGame = false) |> List.length = 1 then OnePlayerStanding
        else if game.deck |> List.isEmpty then DeckOutOfCards
        else Continue

    let playerTakesTurn player deck =
        player.playerNumber |> printfn "Player %i"
        let drawnCard, newDeck= deck |> drawACard
        let validCardChoices = [player.card; drawnCard |> Option.get]
        let selectedCard = userNeedsToChooseACard validCardChoices |> Option.get
        let newCard = validCardChoices |> removeFirstOccurence selectedCard |> List.head
        selectedCard,{player with card=newCard},newDeck

    let rec advancePlayer players =
        match players with
        | h::t ->
            let result = t @ [h]
            if result.Head.isOutOfGame then advancePlayer result else result
        | _ -> []


    let rec playGame game = 
        match shouldGameBeOver game with
            | OnePlayerStanding -> printfn "Player %i won." game.players.Head.playerNumber
            | DeckOutOfCards -> printfn "Game is over because the deck is empty." 
            | Continue ->
                let playedCard,currentPlayer,deck' = playerTakesTurn game.players.Head game.deck
                let nextRound = playCard playedCard {game with players=currentPlayer::game.players.Tail; deck=deck'}
                playGame {nextRound with players=nextRound.players |> advancePlayer}