﻿namespace LoveLetter.Implementation
module Models =
    type Card = | Guard | Priest | Baron | Handmaid | Prince | King | Countess | Princess
    type Deck = Card list
    type Player = {card: Card; playerNumber:int; isProtected:bool; isOutOfGame:bool;}
    type GameCondition = DeckOutOfCards | OnePlayerStanding | Continue
    type Game = {players:Player list; deck:Deck; setAsideCard: Card}

