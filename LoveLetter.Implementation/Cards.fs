﻿namespace LoveLetter.Implementation
module Cards =
    open Models
    open System

    let private numberOfCardsInDeck card =
        match card with
            | Guard -> 5
            | Priest | Baron | Handmaid | Prince -> 2
            | King | Countess | Princess -> 1

    let private buildElements count element = [for i in 1 .. count -> element]
    let cardTypes () = [Guard; Priest; Baron; Handmaid; Prince; King; Countess; Princess]

    let cardPower card =
        cardTypes () |> List.findIndex (fun x -> x = card) |> (+) 1

    let private buildDeck () = cardTypes () |> List.map (fun x -> x |> buildElements (numberOfCardsInDeck x)) |> List.concat

    let private shuffleDeck deck =
        let rand = new System.Random()
        deck |> List.map (fun c -> (rand.Next(), c)) |> List.sortBy fst |> List.map snd

    let prepareDeck () = buildDeck () |> shuffleDeck

    let drawACard deck =
        match deck with
        | head::tail -> Some head, tail
        | _ -> None, []

