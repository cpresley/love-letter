﻿open LoveLetter.Implementation.Prompts
open LoveLetter.Implementation.Players
open LoveLetter.Implementation.Cards
open LoveLetter.Implementation.Game

[<EntryPoint>]
let main argv = 
    let game = initializeGame prepareDeck userNeedsToChooseNumberOfPlayers createPlayer
    match game with 
        | Some x -> playGame x
        | None -> printfn "Couldn't start the game."

    0 // return an integer exit code
